%define __provides_exclude_from %{python3_sitearch}/.*\.so$

%bcond_without samba
%if %{with samba}
%global samba_version %(rpm -q samba-devel --queryformat %{version}-%{release})
%endif

Summary: System Security Services Daemon
Name: sssd
Version: 2.9.4
Release: 5%{?dist}
License: GPLv3+
URL: https://github.com/SSSD/sssd/
Source0: https://github.com/SSSD/sssd/releases/download/%{version}/%{name}-%{version}.tar.gz

# Fix CVE-2023-3758
Patch0001: ad-gpo-use-hash-to-store-intermediate-results.patch

BuildRequires: libtool bind-utils c-ares-devel check-devel cifs-utils-devel
BuildRequires: docbook-style-xsl doxygen gettext-devel dbus-devel krb5-devel
BuildRequires: libnfsidmap-devel libsemanage-devel libsmbclient-devel
BuildRequires: libxslt libunistring libunistring-devel openldap-devel
BuildRequires: libuuid-devel openssl-devel p11-kit-devel pam-devel pcre2-devel
BuildRequires: popt-devel python3-devel systemd-devel
BuildRequires: jansson-devel libcurl-devel libjose-devel bc
%if %{with samba}
BuildRequires: samba-devel samba-winbind
%endif
BuildRequires: systemtap-sdt-devel shadow-utils-subid-devel
BuildRequires: libdhash-devel >= 0.4.2  libfido2-devel
BuildRequires: libini_config-devel >= 1.1
BuildRequires: libldb-devel >= 1.2.0
BuildRequires: softhsm >= 2.1.0
BuildRequires: krb5-libs >= 1.19.1
Requires: sssd-ad = %{version}-%{release}
Requires: sssd-common = %{version}-%{release}
Requires: sssd-ipa = %{version}-%{release}
Requires: sssd-krb5 = %{version}-%{release}
Requires: sssd-ldap = %{version}-%{release}
Requires: sssd-proxy = %{version}-%{release}

%description
Provides a set of daemons to manage access to remote directories and
authentication mechanisms such as LDAP, Kerberos or FreeIPA. It provides
an NSS and PAM interface toward the system and a pluggable back end system
to connect to multiple different account sources.

%package common
Summary: Common files for the SSSD
License: GPLv3+
Requires: libldb >= 1.2.0
Requires: libtevent >= 0.11.0
Requires: sssd-client = %{version}-%{release}
Requires: libsss_sudo = %{version}-%{release}
Requires: sssd-nfs-idmap = %{version}-%{release}
Requires: libsss_idmap = %{version}-%{release}
Requires: libsss_certmap = %{version}-%{release}
%{?systemd_requires}
# libsss_simpleifp is removed starting 2.9.0
Obsoletes: libsss_simpleifp < 2.9.0
Obsoletes: libsss_simpleifp-debuginfo < 2.9.0

%description common
Common files for the SSSD. The common package includes all the files needed
to run a particular backend.

%package ldap
Summary: The LDAP back end of the sssd
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: sssd-krb5-common = %{version}-%{release}
Requires: libsss_idmap = %{version}-%{release}
Requires: libsss_certmap = %{version}-%{release}

%description ldap
Provides the LDAP backend which the sssd can utilize to fetch identity data
from and authenticate against an LDAP server

%package krb5-common
Summary: SSSD helpers needed for Kerberos and GSSAPI authentication
License: GPLv3+
Requires: cyrus-sasl-gssapi
Requires: sssd-common = %{version}-%{release}

%description krb5-common
Provides helper processes which the LDAP and Kerberos backends can use for
Kerberos user or host authentication.

%package krb5
Summary: The Kerberos authentication backend for the sssd
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: sssd-krb5-common = %{version}-%{release}

%description krb5
Provides the Kerberos backend which the SSSD can utilize authenticate
against a Kerberos server

%if %{with samba}
%package common-pac
Summary: Common files supporting PAC processing
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: libsss_idmap = %{version}-%{release}

%description common-pac
Provides common files needed by SSSD providers such as IPA and Active Directory
for handling Kerberos PACs
%endif

%package ipa
Summary: The IPA back end of the SSSD
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: sssd-krb5-common = %{version}-%{release}
Requires: libipa_hbac = %{version}-%{release}
Requires: libsss_certmap = %{version}-%{release}
%if %{with samba}
Requires: samba-client-libs >= %{samba_version}
Requires: sssd-common-pac = %{version}-%{release}
%endif
Requires: libsss_idmap = %{version}-%{release}

%description ipa
Provides the IPA backend which the SSSD can utilize to fetch identity data
from and authenticate against an IPA server.

%package ad
Summary: The AD back end of the SSSD
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: sssd-krb5-common = %{version}-%{release}
%if %{with samba}
Requires: samba-client-libs >= %{samba_version}
Requires: sssd-common-pac = %{version}-%{release}
%endif
Requires: libsss_idmap = %{version}-%{release}
Requires: libsss_certmap = %{version}-%{release}

%description ad
Provides the Active Directory backend that the SSSD can utilize to fetch
identity data from and authenticate against an AD server

%package proxy
Summary: The proxy back end of the SSSD
License: GPLv3+
Requires: sssd-common = %{version}-%{release}

%description proxy
Provides the proxy backend which can be used to wrap an existing NSS and/or
PAM modules to leverage SSSD caching.

%package client
Summary: SSSD Client libraries for NSS and PAM
License: LGPLv3+
Requires: libsss_nss_idmap = %{version}-%{release}
Requires: libsss_idmap = %{version}-%{release}
Requires(post):  /usr/sbin/alternatives
Requires(preun): /usr/sbin/alternatives

%description client
Provides the libraries used to connect to the SSSD service.

%package -n libsss_sudo
Summary: A library for communication between SUDO and SSSD
License: LGPLv3+

%description -n libsss_sudo
A utility library for communication between SUDO and SSSD

%package -n libsss_autofs
Summary: A library for communication between Autofs and SSSD
License: LGPLv3+

%description -n libsss_autofs
A utility library for communication between Autofs and SSSD

%package tools
Summary: Userspace tools for sssd
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: python3-sss = %{version}-%{release}
Requires: python3-sssdconfig = %{version}-%{release}
Requires: libsss_certmap = %{version}-%{release}
Requires: python3-systemd
Requires: sssd-dbus

%description tools
Provides several administrative tools:
sss_debuglevel to change the debug level on the fly
sss_seed which pre-creates a user entry for use in kickstarts
sss_obfuscate for generating an obfuscated LDAP password
sssctl which is an sssd status and control utility

%package -n python3-sssdconfig
Summary: SSSD and IPA configuration file manipulation classes and functions
License: GPLv3+
BuildArch: noarch
Provides: python3-sssdconfig

%description -n python3-sssdconfig
Provides python3 files for manipulation SSSD and IPA configuration files.

%package -n python3-sss
Summary: Python3 bindings for sssd
License: LGPLv3+
Requires: sssd-common = %{version}-%{release}
Provides: python3-sss

%description -n python3-sss
Provides python3 bindings:
function for retrieving list of groups user belongs to class for
obfuscation of passwords

%package -n python3-sss-murmur
Summary: Python3 bindings for murmur hash function
License: LGPLv3+
Provides: python3-sss-murmur

%description -n python3-sss-murmur
Provides python3 module for calculating the murmur hash version 3

%package -n libsss_idmap
Summary: FreeIPA Idmap library
License: LGPLv3+

%description -n libsss_idmap
Utility library to convert SIDs to Unix uids and gids

%package -n libsss_idmap-devel
Summary: FreeIPA Idmap library
License: LGPLv3+
Requires: libsss_idmap = %{version}-%{release}

%description -n libsss_idmap-devel
Utility library to SIDs to Unix uids and gids

%package -n libipa_hbac
Summary: FreeIPA HBAC Evaluator library
License: LGPLv3+

%description -n libipa_hbac
Utility library to validate FreeIPA HBAC rules for authorization requests

%package -n libipa_hbac-devel
Summary: FreeIPA HBAC Evaluator library
License: LGPLv3+
Requires: libipa_hbac = %{version}-%{release}

%description -n libipa_hbac-devel
Utility library to validate FreeIPA HBAC rules for authorization requests

%package -n python3-libipa_hbac
Summary: Python3 bindings for the FreeIPA HBAC Evaluator library
License: LGPLv3+
Requires: libipa_hbac = %{version}-%{release}
Provides: python3-libipa_hbac

%description -n python3-libipa_hbac
The python3-libipa_hbac contains the bindings so that libipa_hbac can be
used by Python applications.

%package -n libsss_nss_idmap
Summary: Library for SID and certificate based lookups
License: LGPLv3+

%description -n libsss_nss_idmap
Utility library for SID and certificate based lookups

%package -n libsss_nss_idmap-devel
Summary: Library for SID and certificate based lookups
License: LGPLv3+
Requires: libsss_nss_idmap = %{version}-%{release}

%description -n libsss_nss_idmap-devel
Utility library for SID and certificate based lookups

%package -n python3-libsss_nss_idmap
Summary: Python3 bindings for libsss_nss_idmap
License: LGPLv3+
Requires: libsss_nss_idmap = %{version}-%{release}
Provides: python3-libsss_nss_idmap

%description -n python3-libsss_nss_idmap
The python3-libsss_nss_idmap contains the bindings so that libsss_nss_idmap can
be used by Python applications.

%package dbus
Summary: The D-Bus responder of the SSSD
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
%{?systemd_requires}

%description dbus
Provides the D-Bus responder of the SSSD, called the InfoPipe, that allows
the information from the SSSD to be transmitted over the system bus.

%package winbind-idmap
Summary: SSSD's idmap_sss Backend for Winbind
License: GPLv3+ and LGPLv3+
Requires: libsss_nss_idmap = %{version}-%{release}
Requires: libsss_idmap = %{version}-%{release}

%description winbind-idmap
The idmap_sss module provides a way for Winbind to call SSSD to map UIDs/GIDs
and SIDs.

%package nfs-idmap
Summary: SSSD plug-in for NFSv4 rpc.idmapd
License: GPLv3+

%description nfs-idmap
The libnfsidmap sssd module provides a way for rpc.idmapd to call SSSD to map
UIDs/GIDs to names and vice versa. It can be also used for mapping principal
(user) name to IDs(UID or GID) or to obtain groups which user are member of.

%package -n libsss_certmap
Summary: SSSD Certificate Mapping Library
License: LGPLv3+

%description -n libsss_certmap
Library to map certificates to users based on rules

%package -n libsss_certmap-devel
Summary: SSSD Certificate Mapping Library
License: LGPLv3+
Requires: libsss_certmap = %{version}-%{release}

%description -n libsss_certmap-devel
Library to map certificates to users based on rules

%package kcm
Summary: An implementation of a Kerberos KCM server
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: krb5-libs >= 1.19.1
%{?systemd_requires}

%description kcm
An implementation of a Kerberos KCM server. Use this package if you want to
use the KCM: Kerberos credentials cache.

%package idp
Summary: Kerberos plugins and OIDC helper for external identity providers.
License: GPLv3+
Requires: sssd-common = %{version}-%{release}

%description idp
This package provides Kerberos plugins that are required to enable
authentication against external identity providers. Additionally a helper
program to handle the OAuth 2.0 Device Authorization Grant is provided.

%package passkey
Summary: SSSD helpers and plugins needed for authentication with passkey token
License: GPLv3+
Requires: sssd-common = %{version}-%{release}
Requires: libfido2

%description passkey
This package provides helper processes and Kerberos plugins that are required to
enable authentication with passkey token.

%prep
%autosetup -p1

%build
autoreconf -ivf

%configure \
    --with-db-path=%{_localstatedir}/lib/sss/db \
    --with-gpo-cache-path=%{_localstatedir}/lib/sss/gpo_cache \
    --with-init-dir=%{_initrddir} \
    --with-initscript=systemd \
    --with-krb5-rcache-dir=%{_localstatedir}/cache/krb5rcache \
    --with-mcache-path=%{_localstatedir}/lib/sss/mc \
    --with-pid-path=%{_rundir} \
    --with-pipe-path=%{_localstatedir}/lib/sss/pipes \
    --with-pubconf-path=%{_localstatedir}/lib/sss/pubconf \
    --with-sssd-user=sssd \
    --with-syslog=journald \
    --with-test-dir=/dev/shm \
    --with-files-provider \
    --with-subid \
    --with-passkey \
%if %{without samba}
    --without-samba \
%endif
    --enable-gss-spnego-for-zero-maxssf \
    --enable-nfsidmaplibdir=%{_libdir}/libnfsidmap \
    --enable-nsslibdir=%{_libdir} \
    --enable-pammoddir=%{_libdir}/security \
    --enable-sss-default-nss-plugin \
    --enable-systemtap \
    --disable-rpath \
    --disable-static \
    --disable-polkit-rules-path \

%make_build all docs

%py3_shebang_fix src/tools/analyzer/sss_analyze
sed -i -e 's:/usr/bin/python:/usr/bin/python3:' src/tools/sss_obfuscate

%install
%make_install
find %{buildroot} -name "*.la" -delete
rm -Rf %{buildroot}/%{_docdir}/%{name}
install -D -m644 src/examples/logrotate %{buildroot}%{_sysconfdir}/logrotate.d/sssd
install -D -m644 src/examples/rwtab %{buildroot}%{_sysconfdir}/rwtab.d/sssd
install -D %{buildroot}/%{_datadir}/sssd-kcm/kcm_default_ccache \
	   %{buildroot}/%{_sysconfdir}/krb5.conf.d/kcm_default_ccache
install -D %{buildroot}/%{_datadir}/sssd/krb5-snippets/enable_sssd_conf_dir \
	   %{buildroot}/%{_sysconfdir}/krb5.conf.d/enable_sssd_conf_dir
install -D %{buildroot}/%{_datadir}/sssd/krb5-snippets/sssd_enable_idp \
	   %{buildroot}/%{_sysconfdir}/krb5.conf.d/sssd_enable_idp
install -D %{buildroot}/%{_datadir}/sssd/krb5-snippets/sssd_enable_passkey \
	   %{buildroot}/%{_sysconfdir}/krb5.conf.d/sssd_enable_passkey

mkdir -p %{buildroot}/%{_sysconfdir}/cifs-utils
%find_lang sssd
touch sssd.lang
for subpackage in sssd_ldap sssd_krb5 sssd_ipa sssd_ad sssd_proxy sssd_tools \
                  sssd_client sssd_dbus sssd_nfs_idmap sssd_winbind_idmap \
                  libsss_certmap sssd_kcm
do
    touch $subpackage.lang
done

for man in `find %{buildroot}/%{_mandir}/??/man?/ -type f | sed -e "s#%{buildroot}/%{_mandir}/##"`
do
    lang=`echo $man | cut -c 1-2`
    case `basename $man` in
        sss_cache*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd.lang
            ;;
        sss_ssh*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd.lang
            ;;
        sss_rpcidmapd*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_nfs_idmap.lang
            ;;
        sss_*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_tools.lang
            ;;
        sssctl*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_tools.lang
            ;;
        sssd_krb5_*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_client.lang
            ;;
        pam_sss*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_client.lang
            ;;
        sssd-ldap*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_ldap.lang
            ;;
        sssd-krb5*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_krb5.lang
            ;;
        sssd-ipa*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_ipa.lang
            ;;
        sssd-ad*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_ad.lang
            ;;
        sssd-proxy*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_proxy.lang
            ;;
        sssd-ifp*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_dbus.lang
            ;;
        sssd-kcm*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_kcm.lang
            ;;
        idmap_sss*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd_winbind_idmap.lang
            ;;
        sss-certmap*)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> libsss_certmap.lang
            ;;
        *)
            echo \%lang\(${lang}\) \%{_mandir}/${man}\* >> sssd.lang
            ;;
    esac
done

%check
export CK_TIMEOUT_MULTIPLIER=10
%make_build check VERBOSE=yes
unset CK_TIMEOUT_MULTIPLIER

%pre common
getent group sssd >/dev/null || groupadd -r sssd
getent passwd sssd >/dev/null || useradd -r -g sssd -d / -s /sbin/nologin -c "User for sssd" sssd

%post common
%systemd_post sssd.service
%systemd_post sssd-autofs.socket
%systemd_post sssd-nss.socket
%if %{with samba}
%systemd_post sssd-pac.socket
%endif
%systemd_post sssd-pam.socket
%systemd_post sssd-pam-priv.socket
%systemd_post sssd-ssh.socket
%systemd_post sssd-sudo.socket

%preun common
%systemd_preun sssd.service
%systemd_preun sssd-autofs.socket
%systemd_preun sssd-nss.socket
%if %{with samba}
%systemd_preun sssd-pac.socket
%endif
%systemd_preun sssd-pam.socket
%systemd_preun sssd-pam-priv.socket
%systemd_preun sssd-ssh.socket
%systemd_preun sssd-sudo.socket

%postun common
%systemd_postun_with_restart sssd-autofs.socket
%systemd_postun_with_restart sssd-nss.socket
%if %{with samba}
%systemd_postun_with_restart sssd-pac.socket
%endif
%systemd_postun_with_restart sssd-pam.socket
%systemd_postun_with_restart sssd-pam-priv.socket
%systemd_postun_with_restart sssd-ssh.socket
%systemd_postun_with_restart sssd-sudo.socket

%systemd_postun sssd-autofs.service
%systemd_postun sssd-nss.service
%if %{with samba}
%systemd_postun sssd-pac.service
%endif
%systemd_postun sssd-pam.service
%systemd_postun sssd-ssh.service
%systemd_postun sssd-sudo.service

%post dbus
%systemd_post sssd-ifp.service

%preun dbus
%systemd_preun sssd-ifp.service

%postun dbus
%systemd_postun_with_restart sssd-ifp.service

%post kcm
%systemd_post sssd-kcm.socket

%preun kcm
%systemd_preun sssd-kcm.socket

%postun kcm
%systemd_postun_with_restart sssd-kcm.socket
%systemd_postun_with_restart sssd-kcm.service

%post client
/usr/sbin/alternatives --install /etc/cifs-utils/idmap-plugin cifs-idmap-plugin %{_libdir}/cifs-utils/cifs_idmap_sss.so 20

%preun client
if [ $1 -eq 0 ] ; then
        /usr/sbin/alternatives --remove cifs-idmap-plugin %{_libdir}/cifs-utils/cifs_idmap_sss.so
fi

%posttrans common
%systemd_postun_with_restart sssd.service

%files
%license COPYING

%files common -f sssd.lang
%license COPYING
%{_unitdir}/sssd.service
%{_unitdir}/sssd-autofs.socket
%{_unitdir}/sssd-autofs.service
%{_unitdir}/sssd-nss.socket
%{_unitdir}/sssd-nss.service
%if %{with samba}
%{_unitdir}/sssd-pac.socket
%{_unitdir}/sssd-pac.service
%endif
%{_unitdir}/sssd-pam.socket
%{_unitdir}/sssd-pam-priv.socket
%{_unitdir}/sssd-pam.service
%{_unitdir}/sssd-ssh.socket
%{_unitdir}/sssd-ssh.service
%{_unitdir}/sssd-sudo.socket
%{_unitdir}/sssd-sudo.service

%dir %{_libexecdir}/sssd
%{_libexecdir}/sssd/sssd_be
%{_libexecdir}/sssd/sssd_nss
%{_libexecdir}/sssd/sssd_pam
%{_libexecdir}/sssd/sssd_autofs
%{_libexecdir}/sssd/sssd_ssh
%{_libexecdir}/sssd/sssd_sudo
%{_libexecdir}/sssd/sss_signal
%{_libexecdir}/sssd/p11_child
%{_libexecdir}/sssd/sssd_check_socket_activated_responders

%dir %{_libdir}/%{name}
%{_libdir}/%{name}/libsss_files.so
%{_libdir}/%{name}/libsss_simple.so
%{_libdir}/%{name}/libsss_child.so
%{_libdir}/%{name}/libsss_crypt.so
%{_libdir}/%{name}/libsss_cert.so
%{_libdir}/%{name}/libsss_debug.so
%{_libdir}/%{name}/libsss_krb5_common.so
%{_libdir}/%{name}/libsss_ldap_common.so
%{_libdir}/%{name}/libsss_util.so
%{_libdir}/%{name}/libsss_semanage.so
%{_libdir}/%{name}/libifp_iface.so
%{_libdir}/%{name}/libifp_iface_sync.so
%{_libdir}/%{name}/libsss_iface.so
%{_libdir}/%{name}/libsss_iface_sync.so
%{_libdir}/%{name}/libsss_sbus.so
%{_libdir}/%{name}/libsss_sbus_sync.so
%dir %{_libdir}/%{name}/conf
%{_libdir}/%{name}/conf/sssd.conf

%{_bindir}/sss_ssh_authorizedkeys
%{_bindir}/sss_ssh_knownhostsproxy
%{_sbindir}/sss_cache
%{_sbindir}/sssd
%(pkg-config --variable=modulesdir ldb)/memberof.so

%dir %{_localstatedir}/lib/sss
%dir %{_localstatedir}/cache/krb5rcache
%attr(700,sssd,sssd) %dir %{_localstatedir}/lib/sss/db
%attr(775,sssd,sssd) %dir %{_localstatedir}/lib/sss/mc
%attr(700,sssd,sssd) %dir %{_localstatedir}/lib/sss/secrets
%attr(751,sssd,sssd) %dir %{_localstatedir}/lib/sss/deskprofile
%ghost %attr(0664,sssd,sssd) %verify(not md5 size mtime) %{_localstatedir}/lib/sss/mc/passwd
%ghost %attr(0664,sssd,sssd) %verify(not md5 size mtime) %{_localstatedir}/lib/sss/mc/group
%ghost %attr(0664,sssd,sssd) %verify(not md5 size mtime) %{_localstatedir}/lib/sss/mc/initgroups
%attr(755,sssd,sssd) %dir %{_localstatedir}/lib/sss/pipes
%attr(750,sssd,root) %dir %{_localstatedir}/lib/sss/pipes/private
%attr(755,sssd,sssd) %dir %{_localstatedir}/lib/sss/pubconf
%attr(755,sssd,sssd) %dir %{_localstatedir}/lib/sss/gpo_cache
%attr(750,sssd,sssd) %dir %{_var}/log/%{name}
%attr(700,sssd,sssd) %dir %{_sysconfdir}/sssd
%attr(711,sssd,sssd) %dir %{_sysconfdir}/sssd/conf.d
%attr(711,root,root) %dir %{_sysconfdir}/sssd/pki
%ghost %attr(600,root,root) %config(noreplace) %{_sysconfdir}/sssd/sssd.conf
%dir %{_sysconfdir}/logrotate.d
%config(noreplace) %{_sysconfdir}/logrotate.d/sssd
%dir %{_sysconfdir}/rwtab.d
%config(noreplace) %{_sysconfdir}/rwtab.d/sssd
%config(noreplace) %{_sysconfdir}/pam.d/sssd-shadowutils

%dir %{_datadir}/sssd
%{_datadir}/sssd/cfg_rules.ini
%dir %{_datadir}/sssd/systemtap
%{_datadir}/sssd/systemtap/id_perf.stp
%{_datadir}/sssd/systemtap/nested_group_perf.stp
%{_datadir}/sssd/systemtap/dp_request.stp
%{_datadir}/sssd/systemtap/ldap_perf.stp
%dir %{_datadir}/systemtap
%dir %{_datadir}/systemtap/tapset
%{_datadir}/systemtap/tapset/sssd.stp
%{_datadir}/systemtap/tapset/sssd_functions.stp

%doc src/examples/sssd-example.conf
%{_mandir}/man1/sss_ssh_authorizedkeys.1*
%{_mandir}/man1/sss_ssh_knownhostsproxy.1*
%{_mandir}/man5/sssd.conf.5*
%{_mandir}/man5/sssd-files.5*
%{_mandir}/man5/sssd-simple.5*
%{_mandir}/man5/sssd-sudo.5*
%{_mandir}/man5/sssd-session-recording.5*
%{_mandir}/man8/sssd.8*
%{_mandir}/man8/sss_cache.8*
%{_mandir}/man5/sssd-systemtap.5*

%files ldap -f sssd_ldap.lang
%license COPYING
%{_libdir}/%{name}/libsss_ldap.so
%{_mandir}/man5/sssd-ldap.5*
%{_mandir}/man5/sssd-ldap-attributes.5*

%files krb5-common
%license COPYING
%attr(4750,root,sssd) %{_libexecdir}/sssd/ldap_child
%attr(4750,root,sssd) %{_libexecdir}/sssd/krb5_child
%attr(755,sssd,sssd) %dir %{_localstatedir}/lib/sss/pubconf/krb5.include.d

%files krb5 -f sssd_krb5.lang
%license COPYING
%{_libdir}/%{name}/libsss_krb5.so
%config(noreplace) %{_sysconfdir}/krb5.conf.d/enable_sssd_conf_dir
%dir %{_datadir}/sssd/krb5-snippets
%{_datadir}/sssd/krb5-snippets/enable_sssd_conf_dir
%{_mandir}/man5/sssd-krb5.5*

%if %{with samba}
%files common-pac
%license COPYING
%{_libexecdir}/sssd/sssd_pac
%endif

%files ipa -f sssd_ipa.lang
%license COPYING
%attr(700,sssd,sssd) %dir %{_localstatedir}/lib/sss/keytabs
%attr(4750,root,sssd) %{_libexecdir}/sssd/selinux_child
%if %{with samba}
%{_libdir}/%{name}/libsss_ipa.so
%{_mandir}/man5/sssd-ipa.5*
%endif

%files ad -f sssd_ad.lang
%license COPYING
%if %{with samba}
%{_libdir}/%{name}/libsss_ad.so
%{_libexecdir}/sssd/gpo_child
%{_mandir}/man5/sssd-ad.5*
%endif

%files proxy
%license COPYING
%attr(4750,root,sssd) %{_libexecdir}/sssd/proxy_child
%{_libdir}/%{name}/libsss_proxy.so

%files dbus -f sssd_dbus.lang
%license COPYING
%{_libexecdir}/sssd/sssd_ifp
%{_unitdir}/sssd-ifp.service
%{_datadir}/dbus-1/system.d/org.freedesktop.sssd.infopipe.conf
%{_datadir}/dbus-1/system-services/org.freedesktop.sssd.infopipe.service
%{_mandir}/man5/sssd-ifp.5*

%files client -f sssd_client.lang
%license src/sss_client/COPYING src/sss_client/COPYING.LESSER
%{_libdir}/libnss_sss.so.2
%{_libdir}/libsubid_sss.so
%{_libdir}/security/pam_sss.so
%{_libdir}/security/pam_sss_gss.so
%{_libdir}/krb5/plugins/libkrb5/sssd_krb5_locator_plugin.so
%if %{with samba}
%{_libdir}/krb5/plugins/authdata/sssd_pac_plugin.so
%endif
%dir %{_libdir}/cifs-utils
%{_libdir}/cifs-utils/cifs_idmap_sss.so
%dir %{_sysconfdir}/cifs-utils
%ghost %{_sysconfdir}/cifs-utils/idmap-plugin
%dir %{_libdir}/%{name}
%dir %{_libdir}/%{name}/modules
%{_libdir}/%{name}/modules/sssd_krb5_localauth_plugin.so
%{_mandir}/man8/pam_sss.8*
%{_mandir}/man8/pam_sss_gss.8*
%{_mandir}/man8/sssd_krb5_locator_plugin.8*
%{_mandir}/man8/sssd_krb5_localauth_plugin.8*

%files -n libsss_sudo
%license src/sss_client/COPYING
%{_libdir}/libsss_sudo.so*

%files -n libsss_autofs
%license src/sss_client/COPYING src/sss_client/COPYING.LESSER
%dir %{_libdir}/%{name}/modules
%{_libdir}/%{name}/modules/libsss_autofs.so

%files tools -f sssd_tools.lang
%license COPYING
%{_sbindir}/sss_obfuscate
%{_sbindir}/sss_override
%{_sbindir}/sss_debuglevel
%{_sbindir}/sss_seed
%{_sbindir}/sssctl
%{_libexecdir}/sssd/sss_analyze
%{python3_sitelib}/sssd/
%{_mandir}/man8/sss_obfuscate.8*
%{_mandir}/man8/sss_override.8*
%{_mandir}/man8/sss_debuglevel.8*
%{_mandir}/man8/sss_seed.8*
%{_mandir}/man8/sssctl.8*

%files -n python3-sssdconfig
%dir %{_datadir}/sssd
%{_datadir}/sssd/sssd.api.conf
%{_datadir}/sssd/sssd.api.d
%dir %{python3_sitelib}/SSSDConfig
%{python3_sitelib}/SSSDConfig/*.py*
%dir %{python3_sitelib}/SSSDConfig/__pycache__
%{python3_sitelib}/SSSDConfig/__pycache__/*.py*
%{python3_sitelib}/*.egg-info

%files -n python3-sss
%{python3_sitearch}/pysss.so

%files -n python3-sss-murmur
%{python3_sitearch}/pysss_murmur.so

%files -n libsss_idmap
%license src/sss_client/COPYING src/sss_client/COPYING.LESSER
%{_libdir}/libsss_idmap.so.*

%files -n libsss_idmap-devel
%{_libdir}/libsss_idmap.so
%{_libdir}/pkgconfig/sss_idmap.pc
%{_includedir}/sss_idmap.h
%doc idmap_doc/html

%files -n libipa_hbac
%license src/sss_client/COPYING src/sss_client/COPYING.LESSER
%{_libdir}/libipa_hbac.so.*

%files -n libipa_hbac-devel
%{_libdir}/libipa_hbac.so
%{_libdir}/pkgconfig/ipa_hbac.pc
%{_includedir}/ipa_hbac.h
%doc hbac_doc/html

%files -n libsss_nss_idmap
%license src/sss_client/COPYING src/sss_client/COPYING.LESSER
%{_libdir}/libsss_nss_idmap.so.*

%files -n libsss_nss_idmap-devel
%doc nss_idmap_doc/html
%{_includedir}/sss_nss_idmap.h
%{_libdir}/libsss_nss_idmap.so
%{_libdir}/pkgconfig/sss_nss_idmap.pc

%files -n python3-libsss_nss_idmap
%{python3_sitearch}/pysss_nss_idmap.so

%files -n python3-libipa_hbac
%{python3_sitearch}/pyhbac.so

%files winbind-idmap -f sssd_winbind_idmap.lang
%if %{with samba}
%dir %{_libdir}/samba/idmap
%{_libdir}/samba/idmap/sss.so
%endif
%{_mandir}/man8/idmap_sss.8*

%files nfs-idmap -f sssd_nfs_idmap.lang
%{_mandir}/man5/sss_rpcidmapd.5*
%{_libdir}/libnfsidmap/sss.so

%files -n libsss_certmap -f libsss_certmap.lang
%license src/sss_client/COPYING src/sss_client/COPYING.LESSER
%{_libdir}/libsss_certmap.so.*
%{_mandir}/man5/sss-certmap.5*

%files -n libsss_certmap-devel
%{_libdir}/libsss_certmap.so
%{_libdir}/pkgconfig/sss_certmap.pc
%{_includedir}/sss_certmap.h
%doc certmap_doc/html

%files kcm -f sssd_kcm.lang
%{_unitdir}/sssd-kcm.socket
%{_unitdir}/sssd-kcm.service
%{_libexecdir}/sssd/sssd_kcm
%config(noreplace) %{_sysconfdir}/krb5.conf.d/kcm_default_ccache
%dir %{_datadir}/sssd-kcm
%{_datadir}/sssd-kcm/kcm_default_ccache
%{_mandir}/man8/sssd-kcm.8*

%files idp
%{_libexecdir}/sssd/oidc_child
%{_libdir}/%{name}/modules/sssd_krb5_idp_plugin.so
%{_datadir}/sssd/krb5-snippets/sssd_enable_idp
%config(noreplace) %{_sysconfdir}/krb5.conf.d/sssd_enable_idp

%files passkey
%config(noreplace) %{_sysconfdir}/krb5.conf.d/sssd_enable_passkey
%attr(755,%{sssd_user},%{sssd_user}) %{_libexecdir}/sssd/passkey_child
%{_libdir}/%{name}/modules/sssd_krb5_passkey_plugin.so
%{_datadir}/sssd/krb5-snippets/sssd_enable_passkey

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.9.4-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.9.4-4
- Rebuilt for loongarch release

* Mon Apr 22 2024 Anakin Zhang <anakinzhang@tencent.com> - 2.9.4-3
- Fix CVE-2023-3758

* Tue Mar 19 2024 Rebuild Robot <rebot@opencloudos.org> - 2.9.4-2
- Rebuilt for jansson

* Fri Jan 26 2024 Anakin Zhang <anakinzhang@tencent.com> - 2.9.4-1
- Upgrade to 2.9.4

* Wed Jan 24 2024 Anakin Zhang <anakinzhang@tencent.com> - 2.9.2-1
- Upgrade to 2.9.2

* Thu Nov 09 2023 kianli <kianli@tencent.com> - 2.9.0-9
- Rebuilt for samba 4.18.8

* Thu Oct 12 2023 Miaojun Dong <zoedong@tencent.com> - 2.9.0-8
- Rebuild for curl-8.4.0

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.9.0-7
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.9.0-6
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 23 2023 kianli@tencent.com - 2.9.0-5
- Rebuilt for bind 9.18.18

* Thu Aug 17 2023 Shuo Wang <abushwang@tencent.com> - 2.9.0-4
- Rebuilt for libunistring 1.1

* Thu Aug 17 2023 cunshunxia <cunshunxia@tencent.com> - 2.9.0-3
- Rebuilt for samba 4.18.5

* Thu Jul 20 2023 cunshunxia <cunshunxia@tencent.com> - 2.9.0-2
- Rebuilt for samba 4.18.4

* Fri Jul 14 2023 Wang Guodong <gordonwwang@tencent.com> - 2.9.0-1
- Upgrade to 2.9.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.8.2-2
- Rebuilt for OpenCloudOS Stream 23.05

* Thu Apr 27 2023 Miaojun Dong <zoedong@tencent.com> - 2.8.2-1
- update version to 2.8.2

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.6.3-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Jul 11 2022 TAO WU <tallwu@tencent.com> - 2.6.3-1
- Initial build.
